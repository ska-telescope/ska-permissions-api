"""Endpoints of the permission service"""

from fastapi import APIRouter
from fastapi.security import OAuth2PasswordBearer
from fastapi_versioning import version

from ska_permissions_api.services.msentra import msgraph_user_profile, requires_auth

v2_router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@v2_router.get("/user/profile")
@version(2)
@requires_auth
async def profile(token: str) -> dict[str]:
    """Fetches the user's profile information from Microsoft Graph API.

    Args:
        token (str): The authentication token.

    Returns:
        dict[str]: A dictionary of user profile information on success.
    """
    user_profile = await msgraph_user_profile(token)
    return {"user_profile": user_profile}
