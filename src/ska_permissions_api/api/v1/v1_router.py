"""Endpoints of the permission service"""

from fastapi import APIRouter, status
from fastapi.security import OAuth2PasswordBearer
from fastapi_versioning import version

from ska_permissions_api.services.msentra import (
    check_user_roles,
    get_msentra_user_group_ids,
    requires_auth,
)

v1_router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@v1_router.get("/getuserrole")
@version(1)
@requires_auth
async def get_user_role(token: str) -> dict[str, bool]:
    """Retrieves the user's roles.

    Args:
        token (str): The authentication token.

    Returns:
        List[str]: A list of user roles on success.
        dict: An error dictionary containing a status code and message on failure.
    """

    try:
        user_roles = await check_user_roles(token)
        return user_roles
    except Exception as exception:  # pylint: disable=broad-exception-caught
        return {"status_code": status.HTTP_500_INTERNAL_SERVER_ERROR, "message": str(exception)}


@v1_router.get("/getusergroupids")
@version(1)
@requires_auth
async def get_user_group_ids(token: str) -> dict[str, list[str]]:
    """Retrieves user group IDs from MS Entra.

    Args:
        token (str): The authentication token.

    Returns:
        dict[str, list[str]]: A dictionary with a list of user group IDs on success.
        dict: An error response containing status code and message on failure.
    """

    try:
        return await get_msentra_user_group_ids(token)
    except Exception as exception:  # pylint: disable=broad-exception-caught
        return {
            "status_code": status.HTTP_500_INTERNAL_SERVER_ERROR,
            "message": str(exception),
        }
