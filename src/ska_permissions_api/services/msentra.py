"""Module to define all the related code for user groups in MS Entra"""

import logging
from typing import Any, Awaitable, Callable

import httpx
from fastapi import Depends, HTTPException, Request

from ska_permissions_api.configuration.constants import constants
from ska_permissions_api.services.ska_portal_permissions import SKAPortalPermissions
from ska_permissions_api.utils.exceptions import AuthError

logger = logging.getLogger(__name__)

ska_portal_permissions = SKAPortalPermissions()


class SKAOUserGroups:
    """SKAO User Groups class that makes use of the user groups defined in MS Entra"""

    def __init__(self, skao_user_group_ids):
        """
        Initializes the SKAOUserGroups with a dictionary of known IDs and their corresponding
        names.

        Args:
            skao_user_group_ids (dict): Dictionary mapping IDs to names.
        """
        self.skao_user_group_ids = skao_user_group_ids
        self.skao_user_group_dict = self.parse_ska_group_permissions(self.skao_user_group_ids)

    def list_user_groups_in_dict(self, data_dict: dict) -> list[str]:
        """Extracts a list of IDs from the provided dictionary.

        Args:
            data_dict (dict): The input dictionary containing IDs.

        Returns:
            list[str]: A list of extracted IDs.
        """
        ids = []
        if "value" in data_dict and isinstance(data_dict["value"], list):
            for item in data_dict["value"]:
                if "id" in item:
                    ids.append(item["id"])
        return ids

    def get_group_names(self, input_ids):
        """
        Compares the input list of group IDs with the known group IDs and returns the matching
        group names.

        Args:
            input_ids (list): List of input IDs to compare.

        Returns:
            list: List of matching names.
        """
        users_groups_names = [
            self.skao_user_group_ids.get(id) for id in input_ids if id in self.skao_user_group_ids
        ]
        return users_groups_names

    def parse_ska_group_permissions(self, user_roles: list[str]):
        """
        This function takes a list of strings in the format "telescope-cluster-service-role"
        and creates a dictionary with indexed roles and their parameters.

        Args:
            user_roles: A list of strings in the format "telescope-cluster-service-role"

        Returns:
            A dictionary with keys of the format "role" and values as dictionaries containing
            'telescope', 'cluster', 'service', and 'role' keys with their respective values parsed
            from the string.
            Raises a ValueError if any format is invalid.
        """
        parsed_roles = {}
        for ms_entra_object_id, group_name in user_roles.items():
            # Split the string on hyphens
            parts = group_name.split("-")

            # Check if there are 4 parts
            if len(parts) == 4:
                parsed_roles[group_name] = {
                    "telescope": parts[0],
                    "cluster": parts[1],
                    "service": parts[2],
                    "role": parts[3],
                }
            else:
                logger.error(
                    "Invalid role format at ms_entra_object_id: %s with group name: %s",
                    ms_entra_object_id,
                    group_name,
                )

        return parsed_roles

    def get_unique_values(self, key: str, data: dict) -> list:
        """
        This function takes a dictionary and a key, and returns a list of unique values for that
        key.

        Args:
            key: The key in the dictionary to extract values from. (str)
            data: The dictionary containing the data. (dict)

        Returns:
            A list containing the unique values found for the specified key. (list)
        """
        values = []
        for _key, value in data.items():  # Use the key from the loop
            try:
                value_name = value[key]
            except KeyError:
                logger.warning("KeyError: %s not found in value dict", key)
                continue
            if value_name not in values:
                values.append(value_name)
        return values

    def get_current_users_group_dict(self, users_groups_names: list[str]):
        """
        Creates a dictionary mapping user groups to their corresponding values from
        skao_user_group_dict.

        Args:
            users_groups_names (list[str]): A list of user group names.

        Returns:
            dict: A dictionary containing user groups as keys and their values from
            skao_user_group_dict.
                    If a group is not found in skao_user_group_dict, it will be omitted from
                    the result.
        """

        users_group_dict = {}
        for group in users_groups_names:
            if group in self.skao_user_group_dict:
                users_group_dict[group] = self.skao_user_group_dict[group]
            else:
                logger.warning("Group %s not found in skao_user_group_dict", group)

        logger.debug("users_group_dict %s", users_group_dict)
        return users_group_dict

    def get_user_permissions(self, users_groups_names: list[str]) -> dict[str, set[str]]:
        """
        Retrieves user permissions based on a list of user group names.

        Args:
            users_groups_names (list[str]): A list of user group names used to filter
                permissions.

        Returns:
            dict[str, set[str]]: A dictionary mapping permission categories
                ("telescope", "cluster", "service", "role") to sets of unique values
                extracted from the user group data. Returns an empty dictionary if
                no permissions are found.
        """

        current_users_group_dict = self.get_current_users_group_dict(users_groups_names)

        # telescopes = self.get_unique_values("telescope", current_users_group_dict)
        telescopes = ["mid"]
        clusters = self.get_unique_values("cluster", current_users_group_dict)
        services = self.get_unique_values("service", current_users_group_dict)
        roles = self.get_unique_values("role", current_users_group_dict)
        formatted_user_groups = {
            "telescope": telescopes,
            "cluster": clusters,
            "service": services,
            "role": roles,
            "portal_top_menu_permissions": ska_portal_permissions.get_unique_numbers(
                users_groups_names
            ),
        }

        return formatted_user_groups


skao_user_groups = SKAOUserGroups(constants.skao_user_group_ids)


async def get_token_auth_header(request: Request):
    """Obtains the Access Token from the Authorization Header"""
    auth = request.headers.get("Authorization", None)
    if not auth:
        raise AuthError("authorization_header_missing: Authorization header is expected.", 401)

    parts = auth.split()

    if parts[0].lower() != "bearer":
        raise AuthError("invalid_header: Authorization header must start with Bearer.", 401)

    if len(parts) == 1:
        raise AuthError("invalid_header: Token not found.", 401)

    if len(parts) > 2:
        raise AuthError("invalid_header: Authorization header must be Bearer token.", 401)

    token = parts[1]
    return token


def requires_auth(f: Callable[..., Awaitable[Any]]) -> Callable[..., Awaitable[Any]]:
    """Decorator to require authentication for a function.

    Args:
        f: The function to decorate.

    Returns:
        The decorated function.
    """

    async def decorated(token: str = Depends(get_token_from_request)) -> Any:
        """Decorated function that handles token validation.

        Args:
            token: The validated access token.

        Returns:
            The result of the decorated function.
        """
        return await f(token=token)

    return decorated


async def get_token_from_request(request: Request) -> str:
    """Extracts and validates an access token from a request.

    Args:
        request: The incoming request.

    Returns:
        The validated access token.

    Raises:
        HTTPException: If token is invalid or unauthorized access.
    """
    try:
        token = await get_token_auth_header(request)
        headers = {"Authorization": f"Bearer {token}"}
        async with httpx.AsyncClient(timeout=10) as client:
            response = await client.get(constants.ms_graph_me_url, headers=headers)
            response.raise_for_status()  # Raise exception for non-200 status codes
        return token
    except httpx.HTTPStatusError as error:
        raise HTTPException(
            status_code=401, detail=f"Token verification failed: {error}"
        ) from error
    except AuthError as error:
        raise HTTPException(status_code=401, detail=f"Invalid token: {error}") from error


async def msgraph_user_profile(token: str) -> dict[str]:
    """Fetches the user's profile information from Microsoft Graph API.

    Args:
        token: The authentication token to access the Microsoft Graph API.

    Returns:
        A dictionary containing the user's profile information if successful,
        otherwise None.

    Raises:
        HTTPException: If there's an issue with the API request or token validation.
    """
    try:
        headers = {"Authorization": f"Bearer {token}"}
        async with httpx.AsyncClient(timeout=10) as client:
            response = await client.get(constants.ms_graph_me_url, headers=headers)
            response.raise_for_status()  # Raise exception for non-200 status codes
        return response.json()
    except httpx.HTTPStatusError as error:
        logger.error("Fetching the user's profile information failed:  %s", error)
        raise HTTPException(
            status_code=401, detail=f"Fetching the user's profile information failed: {error}"
        ) from error
    except AuthError as error:
        logger.error("Invalid token: %s", error)
        raise HTTPException(status_code=401, detail=f"Invalid token: {error}") from error


async def check_user_roles(token: str) -> dict[str, bool]:
    """Checks user roles based on the provided token.

    Args:
        token: The access token.

    Returns:
        A dictionary of user permissions.

    Raises:
        HTTPException: If token is invalid or unauthorized access.
    """
    headers = {"Authorization": f"Bearer {token}"}
    async with httpx.AsyncClient(timeout=10) as client:
        response = await client.get(constants.ms_graph_groups_url, headers=headers)
        response.raise_for_status()  # Raise exception for non-200 status codes
        data = response.json()
        users_groups = skao_user_groups.list_user_groups_in_dict(data)
        users_groups_names = skao_user_groups.get_group_names(users_groups)
        return skao_user_groups.get_user_permissions(users_groups_names)


async def get_msentra_user_group_ids(token: str) -> dict[str, list[str]]:
    """Retrieves user group IDs from MS Entra.

    Args:
        token (str): The authentication token.

    Returns:
        Dict[str, List[str]]: A dictionary containing user group information.
    """
    headers = {"Authorization": f"Bearer {token}"}
    async with httpx.AsyncClient(timeout=10) as client:
        response = await client.get(constants.ms_graph_groups_url, headers=headers)
        response.raise_for_status()  # Raise exception for non-200 status codes
        data = response.json()
        users_groups = skao_user_groups.list_user_groups_in_dict(data)
        return {"user_groups": users_groups}
