"""Class definitions for the SKA Portal"""

# pylint: disable=too-few-public-methods

import logging

logger = logging.getLogger(__name__)


class SKAPortalPermissions:
    """Class object to create all required constants"""

    def __init__(self):
        self.top_menu_permissions_dict: dict = {
            "mid-omc-portal-developer": [0, 1, 2, 3, 5, 6, 7],
            "low-omc-portal-developer": [0, 1, 2, 3, 5, 6, 7],
            "mid-dp-portal-developer": [0, 1, 2, 3, 5, 6, 7],
            "low-dp-portal-developer": [0, 1, 2, 3, 5, 6, 7],
            "mid-itf-portal-aiv": [0, 6, 7],
            "low-itf-portal-aiv": [0, 6, 7],
        }

    def get_unique_numbers(self, users_groups_names: list[str]):
        """
        This function takes a dictionary and one or more keys as arguments.
        It returns a list containing all the unique numbers from the specified keys.

        Args:
            menu_dict: The dictionary containing the menus.
            *keys: One or more keys to retrieve values from the dictionary.

        Returns:
            A list containing all the unique numbers from the specified keys.
        """
        # Initialize an empty set to store unique numbers
        allowed_menu_items = set()

        # Loop through each key provided
        for group_name in users_groups_names:
            logger.debug("group_name %s", group_name)
            # Check if the key exists in the dictionary
            if group_name in self.top_menu_permissions_dict:
                # Add the unique elements from the list associated with the key to the set
                allowed_menu_items.update(self.top_menu_permissions_dict[group_name])

        # Convert the set back to a list and return it
        return list(allowed_menu_items)
