"""Module to define all the constants used as by the service"""

import logging
from pathlib import Path

import ska_ser_logging
from starlette.config import Config

import ska_permissions_api

# pylint: disable=too-few-public-methods

ENV_FILE = Path(".env")
if not ENV_FILE.exists():
    ENV_FILE = None

config = Config(ENV_FILE)

DEBUG: bool = config("API_VERBOSE", cast=bool, default=False)
LOGGING_LEVEL = logging.DEBUG if DEBUG else logging.WARNING
ska_ser_logging.configure_logging(LOGGING_LEVEL)
logger = logging.getLogger(__name__)
logger.info("Logging started for ska_permissions_api at level %s", LOGGING_LEVEL)

SECRETS_FILE = Path("/vault/secrets/config")
if not SECRETS_FILE.exists():
    SECRETS_FILE = None

config = Config(ENV_FILE)
secrets = Config(SECRETS_FILE)


class Constants:  # pylint: disable=too-many-instance-attributes
    """Class object to create all required constants"""

    def __init__(self):
        self.version: str = config(
            "SKA_PERMISSIONS_API_VERSION",
            default=ska_permissions_api.__version__,
        )

        self.msentra_redirect_uri: str = secrets(
            "MSENTRA_REDIRECT_URI",
            default="",
        )
        self.msentra_client_id: str = secrets(
            "MSENTRA_CLIENT_ID",
            default="",
        )
        self.msentra_tenant_id: str = secrets(
            "MSENTRA_TENANT_ID",
            default="",
        )

        self.skao_user_group_ids = {
            "ac9d0e43-931d-455d-b916-7e508db78a8e": "mid-omc-portal-developer",
            "9b2d2f80-0a92-4d91-bcd8-6458b4b127a6": "low-omc-portal-developer",
            "418682b1-a7bc-4008-83a3-af581a8ede3e": "mid-dp-portal-developer",
            "39b6e989-12f3-43d0-b5b8-9fb83f73fb1e": "mid-itf-portal-aiv",
            "f3a9de8e-0966-4c6c-8ce0-f4926aad9d3b": "low-itf-portal-aiv",
        }

        self.ms_graph_url = "https://graph.microsoft.com/v1.0/"
        self.ms_graph_me_url = self.ms_graph_url + "me"
        self.ms_graph_groups_url = (
            self.ms_graph_url + "me/transitiveMemberOf/microsoft.graph.group?$select=ID"
        )


constants = Constants()
