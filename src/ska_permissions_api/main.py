"""SKA Permissions API"""

import logging
import time
from datetime import datetime

from fastapi import Depends, FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi_versioning import VersionedFastAPI

from ska_permissions_api.configuration.constants import constants
from ska_permissions_api.configuration.settings import CORSMiddleware_params
from ska_permissions_api.utils.statistics import Statistics

from .api.v1.v1_router import v1_router
from .api.v2.v2_router import v2_router

statistics = Statistics()


app = FastAPI(
    title="SKAO permissions service API",
    description="This API provides user details and permissions information for SKAO front-end \
applications based on the user's identity in MS Entra.",
    version=constants.version,
    license_info={
        "name": "BSD 3-Clause License",
        "url": "https://gitlab.com/ska-telescope/ska-permissions-api/-/blob/main/LICENSE?ref_type=\
heads",
    },
)

app.include_router(v1_router)
app.include_router(v2_router)


# Set logging to use uvicorn logger.
#
logger = logging.getLogger(__name__)


# Dependencies.
# -------------
async def increment_request_counter() -> None:
    """Increment the request counter"""
    await statistics.increment_request_counter()


@app.get("/status", dependencies=[Depends(increment_request_counter)])
async def status():
    """Service version."""
    return {
        "version": constants.version,
        "request_counter": statistics.request_couter,
        "service_start_time": str(datetime.fromtimestamp(statistics.service_start_time)),
        "uptime": time.time() - statistics.service_start_time,
    }


# This doesn't need authentication
@app.get("/ping")
async def ping():
    """Service aliveness."""
    return {
        "status": "UP",
    }


app = VersionedFastAPI(app, version_format="{major}", prefix_format="/v{major}")
app.add_middleware(CORSMiddleware, **CORSMiddleware_params)
