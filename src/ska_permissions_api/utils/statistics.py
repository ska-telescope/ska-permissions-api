"""Statistics or the SKA Permissions API"""

import asyncio
import time

# pylint: disable=too-few-public-methods


class Statistics:
    """Statistics class"""

    def __init__(self):
        self.service_start_time = time.time()
        self.request_couter = 0
        self.request_couter_lock = asyncio.Lock()

    async def increment_request_counter(self):
        """Increment the request counter"""
        async with self.request_couter_lock:
            self.request_couter += 1
