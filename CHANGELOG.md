# Changelog

## v1.2.0

- [NAL-1287](https://jira.skatelescope.org/browse/NAL-1287)

  - [Changed] Changed component names in chart to only the cart name.
  - [Changed] Changed the service port to port 80.
  - [Changed] Updated base images to latest SKA Python images.
  - [Changed] Updated application to make use of the latest docker file implementation.


## v1.1.0

- [NAL-1191](https://jira.skatelescope.org/browse/NAL-1191)

  - [Added] Added v2 endpoint to ease migration to endpoints that are structured, for example /user/role rather than getuserrole.
  - [Added] Added /v2/user/profile endpoint to obtain user profile details from MS entra.
  - [Changed] Updated Swagger documentation.
  - [Added] Added value files for different clusters for the deployments done with the Gitlab pipeline.
  - [Added] Added recommended labels as per https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/.
  - [Added] Added the verbose logging option to the chart.


- [NAL-1261](https://jira.skatelescope.org/browse/NAL-1261)

  - [Changed] Updated changelog to match guidelines in developer portal.
  - [Added] Added ska-permissions-api chart to enable independent deployment of the SKA Permissions API.
  - [Added] Added the pipeline deployment jobs in the .gitlab.ci file.

## v1.0.1


- [NAL-1228](https://jira.skatelescope.org/browse/NAL-1228)

  - [Changed] Updated application version number to load from environment as default, allowing the status to reflect pipeline tags when images from Gitlab is deployed.

## v1.0.0

- [NAL-1175](https://jira.skatelescope.org/browse/NAL-1175)

  - [Security] Updated Python Docker image to artefact.skao.int/ska-sdp-python:0.1.0 to reduce vulnerabilities.

- [NAL-1146](https://jira.skatelescope.org/browse/NAL-1146)

  - [Changed] Moved the code that checked user roles to new method check_user_roles allowing the requires_auth decorator to only verify that a user can authenticate with MS Entra.
  - [Added] Added getusergroupids endpoint to retrieve user group IDs from MS Entra.

## v0.2.0

- [NAL-1071](https://jira.skatelescope.org/browse/NAL-1071) 
 
  - [Changed] Integrated the ska_ser_logging into the project.
  - [Changed] Retrieves user permissions based on a list of user group names defined in MS Entra.


## v0.1.0

- [NAL-999](https://jira.skatelescope.org/browse/NAL-999) 
 
  - **Breaking** [Change] Addition of the getuserrole endpoint that uses an access token, obtains MS Entra group assignments of the user, and then returns the user roles.
