{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "ska-permissions-api.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Selector labels
Common labels see https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/
*/}}
{{- define "ska-permissions-api.labels" -}}
app.kubernetes.io/name: {{ $.Chart.Name }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
helm.sh/chart: {{ include "ska-permissions-api.chart" . }}
{{- end }}

{{/*
set the ingress url path
*/}}
{{- define "ska-permissions-api.permissionsApi.ingress.path" }}
{{- if .Values.ingress.namespaced -}}
/{{ .Release.Namespace }}/{{ .Values.permissionsApi.ingress.path }}
{{- else -}}
/{{ .Values.permissionsApi.ingress.path }}
{{- end }}
{{- end }}