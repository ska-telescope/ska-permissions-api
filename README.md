# SKAO front-end permissions service
## Description
This repository contains the SKAO front-end permissions service that is used to provide authorisation requests to front end applications.

## Documentation
Please see the latest project documentation on [RTD](https://developer.skao.int/projects/ska-permissions-api/en/latest/?badge=latest)

## Contributing
Contributions are welcome, please see the SKAO developer portal for guidance. https://developer.skao.int/en/latest/

## Project status
In development.
