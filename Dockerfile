# Using multi stage build to update the requirements.txt from the project.toml.
FROM artefact.skao.int/ska-build-python:0.1.1 AS builder

ENV POETRY_NO_INTERACTION=1
ENV POETRY_VIRTUALENVS_IN_PROJECT=1
ENV POETRY_VIRTUALENVS_CREATE=1

WORKDIR /src
COPY pyproject.toml poetry.lock ./

# Install dependencies
RUN poetry install --only main --no-root

COPY src ./src

RUN poetry install --only main

FROM artefact.skao.int/ska-python:0.1.2 AS runner

ENV VIRTUAL_ENV=/src/.venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
ENV PYTHONPATH="$PYTHONPATH:/src/"
ENV PYTHONUNBUFFERED=1
ENV TZ=Etc/UTC

COPY --from=builder ${VIRTUAL_ENV} ${VIRTUAL_ENV}

WORKDIR /src

COPY src/ska_permissions_api ./ska_permissions_api

FROM runner AS prod

CMD ["uvicorn", "ska_permissions_api.main:app", "--reload", "--host", "0.0.0.0", "--port", "8000", "--app-dir", "/usr/src"]
