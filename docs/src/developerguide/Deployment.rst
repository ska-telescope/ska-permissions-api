Deployment Guide
~~~~~~~~~~~~~~~~

The SKA Permissions API is built for continuous operation within a Kubernetes cluster. It functions as a service that enables access permissions services required by users authenticated by MS Entra.


Helm chart configuration options
================================

This section details the configuration options available when deploying the SKA Permissions API in Kubernetes using Helm.

**Ingress**:

.. list-table::
    :widths: 20, 20, 60
    :header-rows: 1

    * - Value
      - Default
      - Comment
    * - ``ingress.enabled``
      - ``true``
      - Whether the Ingress should be enabled.
    * - ``ingress.namespaced``
      - ``true``
      - Whether the namespace should be added to the ingress prefix.
    * - ``ingress.hostname``
      - ``true``
      - The domain name where the application will be hosted. Used for MS Entra redirect URI.

**Permissions API**:

.. list-table::
    :widths: 20, 20, 60
    :header-rows: 1

    * - Value
      - Default
      - Comment
    * - ``permissionsApi.enabled``
      - ``false``
      - If the ska-permissions-api should be enabled.
    * - ``permissionsApi.image.imagePullPolicy``
      - ``IfNotPresent``
      - The image pull policy of the ska-permissions-api.
    * - ``permissionsApi.image.container``
      - ``artefact.skao.int/ska-permissions-api``
      - The link to the artefact repository
    * - ``permissionsApi.image.version``
      - ``1.2.0``
      - The version of the ska-permissions-api to use.
    * - ``permissionsApi.ingress.path``
      - ``"permissionsApi"``
      - What the prefix for the ska-permissions-api path should be.
    * - ``permissionsApi.vault.useVault``
      - ``true``
      - Enables the deployment to retrieve WEB API registration details from the SKAO vault.
    * - ``permissionsApi.vault.pathToSecretVault``
      - ``kv/data/users/andre_odendaal/skao_mf_remote_module_permissions_api``
      - Path to the secrets in the vault.
    * - ``permissionsApi.vault.client_id``
      - ``abcde``
      - Placeholder env variable for MS Entra application registration client ID.
    * - ``permissionsApi.vault.tenant_id``
      - ``abcde``
      - Placeholder env variable for MS Entra application registration tenant ID.
    * - ``permissionsApi.resources.requests.cpu``
      - ``200m``
      - The requested minimum CPU usage of the api.
    * - ``permissionsApi.resources.requests.memory``
      - ``200Mi``
      - The requested minimum memory usage of the api.
    * - ``permissionsApi.resources.limits.cpu``
      - ``1000m``
      - The maximum CPU usage of the api.
    * - ``permissionsApi.resources.limits.memory``
      - ``600Mi``
      - The maximum memory usage of the api.



