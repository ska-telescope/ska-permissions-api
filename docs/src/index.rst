Documentation for the SKAO front-end permissions service API
============================================================

This repository contains the SKAO front-end permissions service that is used to serve the permissions of an authenticated user of a front end application according to the role and group assignment of the user in MS Entra.

.. toctree::
  :maxdepth: 1
  :caption: User Guide

  userguide/overview
  userguide/v1_router
  userguide/v2_router

.. toctree::
  :maxdepth: 1
  :caption: Developer Guide

  developerguide/Development
  developerguide/Deployment

.. toctree::
  :maxdepth: 1
  :caption: Releases

  CHANGELOG.md