SKAO front-end permissions service API Overview
===============================================

This API provides user details and permissions information for SKAO front-end applications based on the user's identity in MS Entra.

Automatic API Documentation
---------------------------

Detailed interactive documentation for the API is available through Swagger UI. For example version 2 of the documentation can be accessed at *http://<API URL>/v2/docs* while running the application.