API Endpoints v2
================


Get user MS Entra profile
~~~~~~~~~~~~~~~~~~~~~~~~~

Fetches the user's profile information from Microsoft Graph API.


*Request*

.. code-block:: bash

    GET /v2/user/profile

    Authorization: Bearer <access_token>


*Response*

.. code-block:: bash

    {
        "user_profile": {
            "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#users/$entity",
            "businessPhones": [],
            "displayName": "Surname, Name",
            "givenName": "Name",
            "jobTitle": "title",
            "mail": "name@org.com",
            "mobilePhone": null,
            "officeLocation": null,
            "preferredLanguage": null,
            "surname": "Surname",
            "userPrincipalName": "surname.name@org.com",
            "id": "aaaaaaaa-bbbb-cccc-dddd-eeeeeeee"
        }
    }









