API Endpoints v1
================


Get user role endpoint
~~~~~~~~~~~~~~~~~~~~~~

Retrieves the user's roles and permissions based on their MS Entra group memberships.

*Request*

.. code-block:: bash

    GET /getuserrole

    Authorization: Bearer <access_token>

*Response*

.. code-block:: bash

    {
        "telescope": ["mid"],
        "cluster": ["omc"],
        "service": ["portal"],
        "role": ["developer"],
        "portal_top_menu_permissions": [0, 1, 2, 3, 5, 6, 7],
    }

*Explanation:*

*telescope, cluster, and service* indicate the user's assigned scopes.
*role* is the primary user role.
*portal_top_menu_permissions* is a list representing allowed actions in the portal. (This implementation is temporary and will be changed in the future)

Get user group IDs
~~~~~~~~~~~~~~~~~~

Retrieves the IDs of the user's MS Entra groups.


*Request*

.. code-block:: bash

    GET /getusergroupids

    Authorization: Bearer <access_token>


*Response*

.. code-block:: bash

    {
        "user_groups": [
            "2d573735-1061-437a-92e5-ceed66f1360f",
            "ac9d0e43-931d-455d-b916-7e508db78a8e",
            "9b2d2f80-0a92-4d91-bcd8-6458b4b127a6",
        ]
    }


