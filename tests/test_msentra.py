"""Tests for msentra.py"""

import pytest

from ska_permissions_api.services.msentra import (
    SKAOUserGroups,
    check_user_roles,
    get_msentra_user_group_ids,
)


def test_parse_ska_group_permissions_valid_roles():
    """
    This test verifies that the function parses a valid list of user roles correctly.
    """
    mock_user_group_ids = {
        "ac9d0e43-931d-455d-b916-7e508db78a8e": "telescope-cluster1-serviceA-role1",
        "9b2d2f80-0a92-4d91-bcd8-6458b4b127a6": "telescope2-cluster2-serviceB-role2",
    }
    ska_user_group_instance = SKAOUserGroups(mock_user_group_ids)

    expected_output = {
        "telescope-cluster1-serviceA-role1": {
            "telescope": "telescope",
            "cluster": "cluster1",
            "service": "serviceA",
            "role": "role1",
        },
        "telescope2-cluster2-serviceB-role2": {
            "telescope": "telescope2",
            "cluster": "cluster2",
            "service": "serviceB",
            "role": "role2",
        },
    }

    parsed_roles = ska_user_group_instance.parse_ska_group_permissions(mock_user_group_ids)
    assert parsed_roles == expected_output


@pytest.mark.asyncio
async def test_check_user_roles(httpx_mock):
    """Test check_user_roles"""
    # Mock response with desired status code and data
    httpx_mock.add_response(
        json={
            "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#groups(id)",
            "value": [
                {"id": "2d573735-1061-437a-92e5-ceed66f1360f"},
                {"id": "ac9d0e43-931d-455d-b916-7e508db78a8e"},
                {"id": "9b2d2f80-0a92-4d91-bcd8-6458b4b127a6"},
            ],
        }
    )

    # Call the function
    users_roles = await check_user_roles("valid_token")

    # Assert expected behavior
    assert users_roles == {
        "telescope": ["mid"],
        "cluster": ["omc"],
        "service": ["portal"],
        "role": ["developer"],
        "portal_top_menu_permissions": [0, 1, 2, 3, 5, 6, 7],
    }


@pytest.mark.asyncio
async def test_get_msentra_user_group_ids(httpx_mock):
    """Test for get_msentra_user_group_ids"""
    httpx_mock.add_response(
        json={
            "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#groups(id)",
            "value": [
                {"id": "2d573735-1061-437a-92e5-ceed66f1360f"},
                {"id": "ac9d0e43-931d-455d-b916-7e508db78a8e"},
                {"id": "9b2d2f80-0a92-4d91-bcd8-6458b4b127a6"},
            ],
        }
    )

    users_groups = await get_msentra_user_group_ids("valid_token")

    assert users_groups == {
        "user_groups": [
            "2d573735-1061-437a-92e5-ceed66f1360f",
            "ac9d0e43-931d-455d-b916-7e508db78a8e",
            "9b2d2f80-0a92-4d91-bcd8-6458b4b127a6",
        ]
    }
