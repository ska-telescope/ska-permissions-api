"""Test for the ska_portal_permissions.py"""

from ska_permissions_api.services.ska_portal_permissions import SKAPortalPermissions


class TestSKAPortalPermissions:
    """Tests for get_unique_numbers"""

    def __init__(self) -> None:
        pass

    def test_get_unique_numbers_single_key(self):
        """
        Test get_unique_numbers with a single valid key
        """
        permissions = SKAPortalPermissions()
        unique_numbers = permissions.get_unique_numbers(["mid-itf-portal-aiv"])
        assert unique_numbers == [0, 6, 7]

    def test_get_unique_numbers_multiple_keys(self):
        """
        Test get_unique_numbers with multiple valid keys
        """
        permissions = SKAPortalPermissions()
        unique_numbers = permissions.get_unique_numbers(
            ["mid-omc-portal-developer", "low-dp-portal-developer"]
        )
        assert unique_numbers == [0, 1, 2, 3, 5, 6, 7]

    def test_get_unique_numbers_empty_keys(self):
        """
        Test get_unique_numbers with no keys provided
        """
        permissions = SKAPortalPermissions()
        unique_numbers = permissions.get_unique_numbers([])
        assert not unique_numbers
