"""Simple FASTAPI App for workshop purposes

Returns:
  None
"""

from fastapi.testclient import TestClient

from ska_permissions_api.main import app

client = TestClient(app)


def test_read_main():
    """Unit test for the root path "/" """
    response = client.get("v1/ping")
    assert response.status_code == 200
    assert response.json() == {"status": "UP"}
