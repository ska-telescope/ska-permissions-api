# pylint: disable=too-few-public-methods
"""Unit tests for the permissions api
"""

import pytest
from fastapi.testclient import TestClient

from ska_permissions_api.configuration.constants import constants
from ska_permissions_api.main import app
from ska_permissions_api.services.msentra import SKAOUserGroups, get_token_auth_header
from ska_permissions_api.utils.exceptions import AuthError

client = TestClient(app)


def test_read_main():
    """Unit test for the root path "/" """
    response = client.get("v1/ping")
    assert response.status_code == 200
    assert response.json() == {"status": "UP"}


class MockRequest:
    """Mock the Request object"""

    def __init__(self, headers):
        self.headers = headers


class TestGetTokenAuthHeader:
    """Test for a token in the header"""

    @pytest.mark.asyncio
    async def test_valid_auth_header(self):
        """Mock request with valid Authorization header"""
        valid_request = MockRequest(headers={"Authorization": "Bearer my_access_token"})
        assert await get_token_auth_header(valid_request) == "my_access_token"

    @pytest.mark.asyncio
    async def test_missing_auth_header(self):
        """Mock request with missing Authorization header"""
        missing_auth_request = MockRequest(headers={})
        with pytest.raises(AuthError, match="authorization_header_missing"):
            await get_token_auth_header(missing_auth_request)

    @pytest.mark.asyncio
    async def test_invalid_header_format(self):
        """Mock request with invalid header format"""
        invalid_format_request = MockRequest(headers={"Authorization": "InvalidFormatToken"})
        with pytest.raises(AuthError, match="invalid_header"):
            await get_token_auth_header(invalid_format_request)

    @pytest.mark.asyncio
    async def test_multiple_parts_in_header(self):
        """Mock request with multiple parts in Authorization header"""
        multiple_parts_request = MockRequest(headers={"Authorization": "Bearer my invalid token"})
        with pytest.raises(AuthError, match="invalid_header"):
            await get_token_auth_header(multiple_parts_request)


class TestSKAOUserGroups:
    """Tests for SKAO User Groups class"""

    def test_find_matches(self):
        """Create an instance of your class (replace with actual instantiation)"""
        my_instance = SKAOUserGroups(constants.skao_user_group_ids)

        # Define some example input IDs and expected matching names
        input_ids = ["39b6e989-12f3-43d0-b5b8-9fb83f73fb1e"]
        expected_names = ["mid-itf-portal-aiv"]

        result = my_instance.get_group_names(input_ids)
        assert result == expected_names, f"Expected {expected_names}, but got {result}"

        # Call the method and check if the result

    def test_extract_ids_from_dict(self):
        """Create an instance of your class (replace with actual instantiation)"""
        my_instance = SKAOUserGroups(constants.skao_user_group_ids)

        # Define some example input data dictionaries
        data_dict_1 = {
            "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#groups(id)",
            "value": [
                {"id": "sdgdsdfd-xcbs-sgsc-vxvr-cvbbfdbdfbvv"},
                {"id": "dfsdvcxc-cvbv-cvcv-bfde-dfgrbffdfbbb"},
                {"id": "hkuykyuj-sdfs-cvbv-b245-cvngththgggg"},
            ],
        }

        # Call the method and check if the extracted IDs match the expected results
        result_1 = my_instance.list_user_groups_in_dict(data_dict_1)
        expected_ids_1 = [
            "sdgdsdfd-xcbs-sgsc-vxvr-cvbbfdbdfbvv",
            "dfsdvcxc-cvbv-cvcv-bfde-dfgrbffdfbbb",
            "hkuykyuj-sdfs-cvbv-b245-cvngththgggg",
        ]
        assert result_1 == expected_ids_1, f"Expected {expected_ids_1}, but got {result_1}"


async def mock_get_token_auth_header(request):
    """Mock the auth token header"""
    return request.token
